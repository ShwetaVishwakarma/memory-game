const gameContainer = document.getElementById("game");
const highScore = document.getElementById("high-score-3");
const score = document.getElementById("score-3");

localStorage.setItem("score-3", 0);
let flip = 0;
score.value = 0;
if (!localStorage.getItem("high-score-3")) {
    localStorage.setItem("high-score-3", 0)
    highScore.value = 0;
} else {
    highScore.value = localStorage.getItem("high-score-3")
}
highScore.textContent = localStorage.getItem("high-score-3")

const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "red",
    "blue",
    "green",
    "orange",
    "purple"
];

const cardArray = [{
        name: 'a',
        img: 'gifs/1.gif'
    },
    {
        name: 'b',
        img: 'gifs/2.gif'
    },
    {
        name: 'c',
        img: 'gifs/3.gif'
    },
    {
        name: 'd',
        img: 'gifs/4.gif'
    },
    {
        name: 'a',
        img: 'gifs/1.gif'
    },
    {
        name: 'b',
        img: 'gifs/2.gif'
    },
    {
        name: 'c',
        img: 'gifs/3.gif'
    },
    {
        name: 'd',
        img: 'gifs/4.gif'
    },
    {
        name: 'a',
        img: 'gifs/1.gif'
    },
    {
        name: 'b',
        img: 'gifs/2.gif'
    },
    {
        name: 'c',
        img: 'gifs/3.gif'
    },
    {
        name: 'd',
        img: 'gifs/4.gif'
    },
    {
        name: 'a',
        img: 'gifs/1.gif'
    },
    {
        name: 'b',
        img: 'gifs/2.gif'
    },
    {
        name: 'c',
        img: 'gifs/3.gif'
    },
    {
        name: 'd',
        img: 'gifs/4.gif'
    },
]

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;
    //console.log(counter);
    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

let shuffledColors = shuffle(cardArray);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(cardArray) {
    for (let i = 0; i < cardArray.length; i++) {
        // create a new div
        const newDiv = document.createElement("img");
        newDiv.setAttribute('src', 'gifs/smiley.jpeg')
        newDiv.setAttribute('dataId', i)
            // give it a class attribute for the value we are looping over
        newDiv.classList.add(cardArray[i].name);

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}

// TODO: Implement this function!
let clickedCard;
var chooseCard = [];
var chooseCardId = [];
var cardsWon = [];


function checkForMatch() {
    var cards = document.querySelectorAll('img')
        // cards.forEach(card => card.addEventListener("click", flipCard));

    // function flipCard() {
    //     this.classList.toggle("flip")
    // }
    // console.log(cards);
    const optionOneId = chooseCardId[0];
    //console.log(optionOneId);
    //console.log("Hi");
    const optionTwoId = chooseCardId[1];
    //console.log(optionTwoId);

    if (chooseCard[0] === chooseCard[1] && chooseCardId[0] !== chooseCardId[1]) {
        score.value += 200 - (flip * 10);
        flip = 0;
        score.textContent = score.value;
        localStorage.setItem("score-3", score.value);
        if (score.value > highScore.value) {
            highScore.value = score.value;
            localStorage.setItem("high-score-3", highScore.value);
            highScore.textContent = highScore.value;
        }
        cards[optionOneId].removeEventListener("click", handleCardClick);
        cards[optionTwoId].removeEventListener("click", handleCardClick);
        cardsWon.push(chooseCard)
            // console.log(cardsWon);
    } else {
        cards[optionOneId].setAttribute('src', 'gifs/smiley.jpeg')
        cards[optionTwoId].setAttribute('src', 'gifs/smiley.jpeg')
        console.log("Not Matched");
    }
    chooseCard = [];
    chooseCardId = [];
}
let moves = 0;

function handleCardClick(event) {
    if (moves >= 20) {
        window.location = "endPage.html"
    } else {
        moves++;
    }
    flip += 1;
    //console.log(count);
    // you can use event.target to see which element was clicked
    // console.log("you clicked", event.target);
    var cardId = this.getAttribute('dataId')
        // console.log(cardId);
    chooseCard.push(cardArray[cardId].name)
        // console.log(chooseCard);
    chooseCardId.push(cardId);
    event.target.classList.add("flip")

    this.setAttribute('src', cardArray[cardId].img)
    if (chooseCard.length === 2) {
        setTimeout(checkForMatch, 1000)
    }
}
startTimer();
const timer = document.getElementById("timer");

function startTimer() {
    let time = 45;
    let interval = setInterval(function() {
        timer.textContent = time--;
        if (time < 0) {
            clearInterval(interval)
            window.location = "endPage.html"
        }
    }, 1000)
}



// when the DOM loads
createDivsForColors(shuffledColors);